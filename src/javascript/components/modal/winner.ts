import { FighterDetails } from '../../types/fighter.type';

export function showWinnerModal(fighter: FighterDetails) {
  // call showModal function

  const winnerInfo = {
    title: '...And The WINNER!',
    bodyElement: fighter.name,
  };
}
